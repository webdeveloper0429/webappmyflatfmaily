(function () {
    'use strict';

    var HeaderComponent = {
        bindings: {
        },
        templateUrl: 'components/header/header.template.html',
        controller: 'HeaderComponentCtrl',
        controllerAs: 'vm'
    };

    HeaderComponentCtrl.$inject = [];

    function HeaderComponentCtrl() {
        var vm = this;

    }

    angular
        .module('myApp')
        .component('headerComponent', HeaderComponent)
        .controller('HeaderComponentCtrl', HeaderComponentCtrl);
})();