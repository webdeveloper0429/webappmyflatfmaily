(function () {
    'use strict';

    var CustomPopupComponent = {
        bindings: {
            title: '@',
            text: '@',
            closeText: '@',
            successText: '@',
            isShowCloseIcon: '<',
            isShowDarkBackground: '<',
            onCloseEvent: '&',
            onSuccessEvent: '&'
        },
        templateUrl: 'components/customPopup/customPopup.template.html',
        controller: 'CustomPopupComponentCtrl',
        controllerAs: 'vm'
    };

    CustomPopupComponentCtrl.$inject = [];

    function CustomPopupComponentCtrl() {
        var vm = this;

    }

    angular
        .module('myApp')
        .component('customPopupComponent', CustomPopupComponent)
        .controller('CustomPopupComponentCtrl', CustomPopupComponentCtrl);
})();