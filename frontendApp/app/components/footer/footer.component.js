(function () {
    'use strict';

    var FooterComponent = {
        bindings: {
        },
        templateUrl: 'components/footer/footer.template.html',
        controller: 'FooterComponentCtrl',
        controllerAs: 'vm'
    };

    FooterComponentCtrl.$inject = [];

    function FooterComponentCtrl() {
        var vm = this;

    }

    angular
        .module('myApp')
        .component('footerComponent', FooterComponent)
        .controller('FooterComponentCtrl', FooterComponentCtrl);
})();