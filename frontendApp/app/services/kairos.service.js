(function () {
    'use strict';

    angular
        .module('myApp')
        .factory('KairosService', KairosService);

    KairosService.$inject = ['$resource'];

    function KairosService($resource) {
        var apiUrl = "https://api.kairos.com/";

        return {
            detect: $resource(apiUrl + 'detect', {},
                {
                    save: {method: 'POST',
                        headers: { 'Content-Type' : 'application/json', "app_id": "4985f625", "app_key": "4423301b832793e217d04bc44eb041d3" } }
                })

        };
    }
})();