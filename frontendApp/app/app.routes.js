angular
    .module('myApp')
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider',

        function ($stateProvider, $urlRouterProvider, $locationProvider) {
        'use strict';

        //$locationProvider.html5Mode(true);

        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("/home");

        // Now set up the states
        $stateProvider
            .state('home', {
                url: '/home',
                controller: 'HomeCtrl',
                controllerAs: 'vm',
                templateUrl: 'views/home/home.html'
            })
            .state('orderInfo', {
                url: '/orderInfo',
                controller: 'OrderInfoCtrl',
                controllerAs: 'vm',
                templateUrl: 'views/orderInfo/orderInfo.html'
            })
            .state('printSelect', {
                url: '/printSelect',
                controller: 'PrintSelectCtrl',
                controllerAs: 'vm',
                templateUrl: 'views/printSelect/printSelect.html'
            })
            .state('demo', {
                url: '/demo',
                controller: 'DemoCtrl',
                controllerAs: 'vm',
                templateUrl: 'views/demo/demo.html'
            })
            .state('magicEdge', {
                url: '/magicEdge',
                controller: 'MagicEdgeCtrl',
                controllerAs: 'vm',
                templateUrl: 'views/magicEdge/magicEdge.html'
            })
            .state('halftone', {
                url: '/halftone',
                controller: 'HalftoneCtrl',
                controllerAs: 'vm',
                templateUrl: 'views/halftone/halftone.html'
            })
        }
    ]);
