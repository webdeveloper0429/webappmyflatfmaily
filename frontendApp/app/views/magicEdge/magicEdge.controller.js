(function () {
    "use strict";

    angular
        .module("myApp")
        .controller("MagicEdgeCtrl", MagicEdgeCtrl);

    MagicEdgeCtrl.$inject = [];

    function MagicEdgeCtrl() {
        var vm = this;

        initializeMagicEdge();

        // jQuery(document).ready(function() {
        //     initializeMagicEdge();
        // });

        function initializeMagicEdge(){
            var me = new MagicEdge();
            me.init(document.getElementById("MagicEdgeHolder"));
        }

        // var a = new MagicEdge();
        // a.setConfig({
        //     width:"full",
        //     height:"full",
        //     zoomMultipliers: [0.1, 0.2, 0.4, 0.7, 1, 1.5, 2, 2.5, 3, 4, 6, 8, 10, 14, 20],
        //     saveNameSufix:"transparent-",
        //     saveAction: "download",
        //     ajaxUrl: "ajax.php",
        //     magicWandTolerance:5,
        //     magicWandBorderWidth:10
        // });
        // a.init(document.getElementById("MagicEdgeHolder"));
    }
})();