(function () {
    "use strict";

    angular
        .module("myApp")
        .controller("PrintSelectCtrl", PrintSelectCtrl);

    PrintSelectCtrl.$inject = [];

    function PrintSelectCtrl() {
        var vm = this;

        vm.selectPrint = selectPrint;

        vm.printMerchandiseList = [
            {
                title: 'Apparel',
                imageSrc: 'img/apparel.png',
                price: 38.85,
                colourList: [
                    {
                        title: 'Select colour', value: 0
                    },
                    {
                        title: 'Pink', value: 1
                    },
                    {
                        title: 'Red', value: 2
                    },
                    {
                        title: 'Orange', value: 3
                    },
                    {
                        title: 'Yellow', value: 4
                    },
                    {
                        title: 'Brown', value: 5
                    },
                    {
                        title: 'Green', value: 6
                    },
                    {
                        title: 'Cyan', value: 7
                    },
                    {
                        title: 'Blue', value: 8
                    }
                ],
                selectedColour: 0,
                isSelected: false
            },
            {
                title: 'Magnets',
                imageSrc: 'img/magnet.png',
                price: 9.55,
                colourList: [
                    {
                        title: 'Select colour', value: 0
                    },
                    {
                        title: 'Pink', value: 1
                    },
                    {
                        title: 'Red', value: 2
                    },
                    {
                        title: 'Orange', value: 3
                    },
                    {
                        title: 'Yellow', value: 4
                    },
                    {
                        title: 'Brown', value: 5
                    },
                    {
                        title: 'Green', value: 6
                    },
                    {
                        title: 'Cyan', value: 7
                    },
                    {
                        title: 'Blue', value: 8
                    }
                ],
                selectedColour: 0,
                isSelected: false
            },
            {
                title: 'Window Decals',
                imageSrc: 'img/window-decal.png',
                price: 12.95,
                colourList: [
                    {
                        title: 'Select colour', value: 0
                    },
                    {
                        title: 'Pink', value: 1
                    },
                    {
                        title: 'Red', value: 2
                    },
                    {
                        title: 'Orange', value: 3
                    },
                    {
                        title: 'Yellow', value: 4
                    },
                    {
                        title: 'Brown', value: 5
                    },
                    {
                        title: 'Green', value: 6
                    },
                    {
                        title: 'Cyan', value: 7
                    },
                    {
                        title: 'Blue', value: 8
                    }
                ],
                selectedColour: 0,
                isSelected: false
            },
            {
                title: 'Totes',
                imageSrc: 'img/totes.png',
                price: 45.05,
                colourList: [
                    {
                        title: 'Select colour', value: 0
                    },
                    {
                        title: 'Pink', value: 1
                    },
                    {
                        title: 'Red', value: 2
                    },
                    {
                        title: 'Orange', value: 3
                    },
                    {
                        title: 'Yellow', value: 4
                    },
                    {
                        title: 'Brown', value: 5
                    },
                    {
                        title: 'Green', value: 6
                    },
                    {
                        title: 'Cyan', value: 7
                    },
                    {
                        title: 'Blue', value: 8
                    }
                ],
                selectedColour: 0,
                isSelected: false
            }
        ];

        init();
        function init() {
            for (var i = 0; i < vm.printMerchandiseList.length; i++) {
                vm.printMerchandiseList[i].selectedColour = vm.printMerchandiseList[i].colourList[0];
            }
        }
        
        function selectPrint(index) {
            vm.printMerchandiseList[index].isSelected = !vm.printMerchandiseList[index].isSelected;
        }
    }
})();