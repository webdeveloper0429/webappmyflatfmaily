(function () {
    "use strict";

    angular
        .module("myApp")
        .controller("OrderInfoCtrl", OrderInfoCtrl);

    OrderInfoCtrl.$inject = [];

    function OrderInfoCtrl() {
        var vm = this;

        vm.removeOrder = removeOrder;
        vm.getTotal = getTotal;
        
        vm.orderList = [
            {
                previewUrl: 'img/boy-basic.png',
                category: 'boy',
                style: 'boy basic',
                price: 12.95
            },
            {
                previewUrl: 'img/girl-basic.png',
                category: 'girl',
                style: 'girl basic',
                price: 12.95
            },
            {
                previewUrl: 'img/dog-basic.png',
                category: 'dog',
                style: 'dog basic',
                price: 12.95
            }
        ];
        
        function removeOrder(index) {
            vm.orderList.splice(index, 1);
        }

        function getTotal() {
            var sum = 0;
            for (var i = 0; i < vm.orderList.length; i++) {
                sum += vm.orderList[i].price;
            }
            return sum.toFixed(2);
        }


    }
})();