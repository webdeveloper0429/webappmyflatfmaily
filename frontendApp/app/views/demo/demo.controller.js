(function () {
    "use strict";

    angular
        .module("myApp")
        .controller("DemoCtrl", DemoCtrl);

    DemoCtrl.$inject = ["$scope", "KairosService"];

    function DemoCtrl($scope, KairosService) {

        var vm = this;

        var creds = {
            kairos: {
                appName: "VALEDA's App",
                appID: "ac5f6143",
                appKey: "9bd023e5acd39ae7deda552f052fabf2"
            },
            cloudinary: {
                cloudName: "valeda",
                apiKey: "923832978174659",
                apiSecret: "l0S3CHccDIwnBhUsc0fHOxdILaE"
            }
        };

        vm.imageSrc = null;
        vm.isLoading = false;

        vm.canvasInfo = {
            canvas: document.getElementById('myCanvas'),
            ctx: document.getElementById('myCanvas').getContext('2d'),
            image: null,
            ratio: 0,
            multiplierX: 0,
            multiplierY: 0
        };

        vm.getBackgroundStyle = getBackgroundStyle;
        vm.recogniseFaces = recogniseFaces;



        // start Picture Preview
        $scope.imageUpload = function (what, event) {
            var files = event.target.files;

            if (what === "image") {
                var file = files[0];

                var img = new Image;
                img.src = URL.createObjectURL(file);

                img.onload = function() {
                    vm.canvasInfo.ctx.clearRect(0, 0, vm.canvasInfo.canvas.width, vm.canvasInfo.canvas.height);

                    var rotX = vm.canvasInfo.canvas.width / img.width;
                    var rotY = vm.canvasInfo.canvas.height / img.height;

                    if (img.width > img.height) {
                        vm.canvasInfo.ratio = rotX;
                    } else {
                        vm.canvasInfo.ratio = rotY;
                    }

                    vm.canvasInfo.ctx.drawImage(img, 0,0, img.width * vm.canvasInfo.ratio,
                        img.height * vm.canvasInfo.ratio);

                    vm.canvasInfo.image = img;
                    vm.canvasInfo.multiplierX = img.width / 100;
                    vm.canvasInfo.multiplierY = img.height / 100;
                }

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(file);
            }
        }

        function imageIsLoaded (e) {
            $scope.$apply(function () {
                vm.imageSrc = e.target.result;
            });
        }

        function getBackgroundStyle () {
            return {
                'background-image': 'url(' + vm.imageSrc + ')'
            }
        }
        
        
        function recogniseFaces() {
            vm.isLoading = true;
            vm.canvasInfo.ctx.clearRect(0, 0, vm.canvasInfo.canvas.width, vm.canvasInfo.canvas.height);

            vm.canvasInfo.ctx.drawImage(vm.canvasInfo.image, 0,0, vm.canvasInfo.image.width * vm.canvasInfo.ratio,
                vm.canvasInfo.image.height * vm.canvasInfo.ratio);
            vm.canvasInfo.ctx.strokeStyle = '#ff0000';

            var data = {
                "image":vm.imageSrc
            };

            return KairosService.detect.save(data).$promise
                .then(function success(data) {
                    vm.isLoading = false;

                    if (data.images && data.images.length && data.images[0].faces.length) {

                        for(var i = 0; i < data.images[0].faces.length; i++) {
                            var topLeftX = (data.images[0].faces[i].topLeftX - vm.canvasInfo.multiplierX * 7)
                                * vm.canvasInfo.ratio;
                            var topLeftY = (data.images[0].faces[i].topLeftY - vm.canvasInfo.multiplierY * 20)
                                * vm.canvasInfo.ratio;
                            var width = (data.images[0].faces[i].width + vm.canvasInfo.multiplierX * 17)
                                * vm.canvasInfo.ratio;
                            var height = (data.images[0].faces[i].height + vm.canvasInfo.multiplierX * 20)
                                * vm.canvasInfo.ratio;

                            vm.canvasInfo.ctx.rect(topLeftX,topLeftY,width,height);
                            vm.canvasInfo.ctx.stroke();



                            var cropCanvasCtx = document.getElementById('myCanvasCrop').getContext('2d');

                            cropCanvasCtx.drawImage(vm.canvasInfo.image,
                                topLeftX, topLeftY,   // Start at 10 pixels from the left and the top of the image (crop),
                                width * 1.5, height * 1.5,   // "Get" a `80 * 30` (w * h) area from the source image (crop),
                                0, 0,     // Place the result at 0, 0 in the canvas,
                                width, height); // With as width / height: 160 * 60 (scale)

                            var imgd = cropCanvasCtx.getImageData(0, 0, width, height);
                            var pix = imgd.data;
                            for (var i = 0, n = pix.length; i < n; i += 4) {
                                var grayscale = pix[i ] * .3 + pix[i+1] * .59 + pix[i+2] * .11;
                                pix[i ] = grayscale; // red
                                pix[i+1] = grayscale; // green
                                pix[i+2] = grayscale; // blue
                            }
                            cropCanvasCtx.putImageData(imgd, 0, 0);

                        }
                    }

                    return data;
                })
                .catch(function apiError(error) {

                    return false;
                });
        }

    }
})();