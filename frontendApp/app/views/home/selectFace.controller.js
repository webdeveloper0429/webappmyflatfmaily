(function () {
    'use strict';

    var SelectFaceComponent = {
        bindings: {
            originalImageFile: '=',
            originalFace: "=",
            onPreviousStep: '&',
            onNextStep: '&'
        },
        templateUrl: 'views/home/selectFace.template.html',
        controller: 'SelectFaceCtrl',
        controllerAs: 'vm'
    };

    SelectFaceCtrl.$inject = ['$timeout'];

    function SelectFaceCtrl($timeout) {
        var vm = this;

        vm.magicEdge = null;
        vm.convertedImageBase64 = null;


        vm.convertImage = convertImage;

        initializeMagicEdge();

        function initializeMagicEdge(){
            vm.magicEdge = new MagicEdge();
            vm.magicEdge.init(document.getElementById("MagicEdgeHolder"));

            if (vm.originalImageFile) {
                var f = vm.originalImageFile;
                var url = window.URL || window.webkitURL;
                var imgurl = url.createObjectURL(f);

                vm.magicEdge.brApplication.loadImage.apply(vm.magicEdge.brApplication, [imgurl]);
            }
        }

        function convertImage () {
            var outputConfig = {
                backgroundColor: "#FFFFFF",
                drawBackground: false,
                jpgQuality: "75",
                mime: "image/png",
                type: "png"
            };

            vm.magicEdge.brApplication.createTransparentImageCanvas();

            var outputCanvas = vm.magicEdge.brApplication.getOutputImageCanvas();

            vm.convertedImageBase64 = outputCanvas.toDataURL(outputConfig.mime, undefined);
            vm.originalFace = vm.convertedImageBase64;

            $timeout(function () {
                vm.onNextStep();
            }, 10);
        }

    }

    angular
        .module('myApp')
        .component('selectFaceComponent', SelectFaceComponent)
        .controller('SelectFaceCtrl', SelectFaceCtrl);
})();