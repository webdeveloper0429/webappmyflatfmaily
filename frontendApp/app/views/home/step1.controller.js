(function () {
    'use strict';

    var HomeStepOneComponent = {
        bindings: {
            originalImageFile: '=',
            onNextStep: '&'
        },
        templateUrl: 'views/home/step1.template.html',
        controller: 'HomeStepOneCtrl',
        controllerAs: 'vm'
    };

    HomeStepOneCtrl.$inject = ['$scope'];

    function HomeStepOneCtrl($scope) {
        var vm = this;

        vm.convertedPhoto = null;
        vm.imageSrc = null;

        vm.getBackgroundStyle = getBackgroundStyle;
        vm.removeImage = removeImage;
        vm.convertPhoto = convertPhoto;
        vm.clearImages = clearImages;

        // start Picture Preview
        $scope.imageUpload = function (event) {
            var files = event.target.files;

            var file = (files && files.length) ? files[0] : null;
            vm.originalImageFile = file;

            if (file) {
                var img = new Image;
                img.src = URL.createObjectURL(file);
                // vm.originalImage = img.src;

                // img.onload = function() {
                //     vm.canvasInfo.ctx.clearRect(0, 0, vm.canvasInfo.canvas.width, vm.canvasInfo.canvas.height);
                //
                //     var rotX = vm.canvasInfo.canvas.width / img.width;
                //     var rotY = vm.canvasInfo.canvas.height / img.height;
                //
                //     if (img.width > img.height) {
                //         vm.canvasInfo.ratio = rotX;
                //     } else {
                //         vm.canvasInfo.ratio = rotY;
                //     }
                //
                //     vm.canvasInfo.ctx.drawImage(img, 0,0, img.width * vm.canvasInfo.ratio,
                //         img.height * vm.canvasInfo.ratio);
                //
                //     vm.canvasInfo.image = img;
                //     vm.canvasInfo.multiplierX = img.width / 100;
                //     vm.canvasInfo.multiplierY = img.height / 100;
                // }

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(file);
            }
        }

        function imageIsLoaded (e) {
            $scope.$apply(function () {
                vm.imageSrc = e.target.result;
            });
        }

        function getBackgroundStyle () {
            return {
                'background-image': 'url(' + vm.imageSrc + ')'
            }
        }

        function removeImage() {
            vm.imageSrc = null;
        }

        function convertPhoto() {
            vm.convertedPhoto = {};
        }

        function clearImages() {
            vm.convertedPhoto = null;
            vm.imageSrc = null;
        }
    }

    angular
        .module('myApp')
        .component('homeStepOneComponent', HomeStepOneComponent)
        .controller('HomeStepOneCtrl', HomeStepOneCtrl);
})();