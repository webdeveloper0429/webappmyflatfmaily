(function () {
    "use strict";

    angular
        .module("myApp")
        .controller("HomeCtrl", HomeCtrl);

    HomeCtrl.$inject = [];

    function HomeCtrl() {
        var vm = this;

        vm.originalImageFile = null;
        vm.originalFace = null;
        vm.filterFace = null;

        vm.isShow = {
            step1: false,
            selectFace: false,
            faceFilter: false,
            step2: false
        };

        vm.showPopup = showPopup;

        init();
        function init() {
            vm.originalImage = null;
            vm.originalFace = null;
            vm.filterFace = null;

            vm.isShow = {
                step1: false,
                selectFace: false,
                faceFilter: false,
                step2: false,
            };

            showPopup('step1');
        }
        
        function showPopup(what) {
            vm.isShow = {
                step1: false,
                selectFace: false,
                faceFilter: false,
                step2: false,
            };

            switch (what) {
                case 'step1':
                    vm.isShow.step1 = true;
                    break;

                case 'select-face':
                    vm.isShow.selectFace = true;
                    break;

                case 'face-filter':
                    vm.isShow.faceFilter = true;
                    break;

                case 'step2':
                    vm.isShow.step2 = true;
                    break;

                default:
            }
        }


    }
})();