(function () {
    'use strict';

    var HomeStepTwoComponent = {
        bindings: {
            filterFace: '=',
            onPreviousStep: '&'
        },
        templateUrl: 'views/home/step2.template.html',
        controller: 'HomeStepTwoCtrl',
        controllerAs: 'vm'
    };

    HomeStepTwoCtrl.$inject = ['$scope'];

    function HomeStepTwoCtrl($scope) {
        var vm = this;

        vm.categoryList = [
            {
                title: 'adult male',
                value: 0,
                bodyStyleList: [
                {
                    title: 'Man basic',
                    value: 0,
                    body: 'img/Man.svg'
                },
                {
                    title: 'Man football',
                    value: 1,
                    body: 'img/Man.svg'
                },
                {
                    title: 'Man baseball',
                    value: 2,
                    body: 'img/Man.svg'
                },
                {
                    title: 'Man tennis',
                    value: 3,
                    body: 'img/Man.svg'
                },
                {
                    title: 'Man swimming',
                    value: 4,
                    body: 'img/Man.svg'
                },
                {
                    title: 'Man backetball',
                    value: 5,
                    body: 'img/Man.svg'
                },
                {
                    title: 'Man ice skating',
                    value: 6,
                    body: 'img/Man.svg'
                },
                {
                    title: 'Man soccer',
                    value: 7,
                    body: 'img/Man.svg'
                },
                {
                    title: 'Man hockey',
                    value: 8,
                    body: 'img/Man.svg'
                }
            ]
            },
            {
                title: 'adult female',
                value: 1,
                bodyStyleList: [
                    {
                        title: 'Woman basic',
                        value: 0,
                        body: 'img/woman.svg'
                    },
                    {
                        title: 'Woman football',
                        value: 1,
                        body: 'img/woman.svg'
                    },
                    {
                        title: 'Woman baseball',
                        value: 2,
                        body: 'img/woman.svg'
                    },
                    {
                        title: 'Woman tennis',
                        value: 3,
                        body: 'img/woman.svg'
                    },
                    {
                        title: 'Woman swimming',
                        value: 4,
                        body: 'img/woman.svg'
                    },
                    {
                        title: 'Woman backetball',
                        value: 5,
                        body: 'img/woman.svg'
                    },
                    {
                        title: 'Woman ice skating',
                        value: 6,
                        body: 'img/woman.svg'
                    },
                    {
                        title: 'Woman soccer',
                        value: 7,
                        body: 'img/woman.svg'
                    },
                    {
                        title: 'Woman hockey',
                        value: 8,
                        body: 'img/woman.svg'
                    }
                ]
            },
            {
                title: 'teen boy',
                value: 2,
                bodyStyleList: [
                    {
                        title: 'boy basic',
                        value: 0,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy football',
                        value: 1,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy baseball',
                        value: 2,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy tennis',
                        value: 3,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy swimming',
                        value: 4,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy backetball',
                        value: 5,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy ice skating',
                        value: 6,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy soccer',
                        value: 7,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy hockey',
                        value: 8,
                        body: 'img/body.jpg'
                    }
                ]
            },
            {
                title: 'teen girl',
                value: 3,
                bodyStyleList: [
                    {
                        title: 'boy basic',
                        value: 0,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy football',
                        value: 1,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy baseball',
                        value: 2,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy tennis',
                        value: 3,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy swimming',
                        value: 4,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy backetball',
                        value: 5,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy ice skating',
                        value: 6,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy soccer',
                        value: 7,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy hockey',
                        value: 8,
                        body: 'img/body.jpg'
                    }
                ]
            },
            {
                title: 'boy',
                value: 4,
                bodyStyleList: [
                    {
                        title: 'boy basic',
                        value: 0,
                        body: 'img/boy.svg'
                    },
                    {
                        title: 'boy football',
                        value: 1,
                        body: 'img/boy.svg'
                    },
                    {
                        title: 'boy baseball',
                        value: 2,
                        body: 'img/boy.svg'
                    },
                    {
                        title: 'boy tennis',
                        value: 3,
                        body: 'img/boy.svg'
                    },
                    {
                        title: 'boy swimming',
                        value: 4,
                        body: 'img/boy.svg'
                    },
                    {
                        title: 'boy backetball',
                        value: 5,
                        body: 'img/boy.svg'
                    },
                    {
                        title: 'boy ice skating',
                        value: 6,
                        body: 'img/boy.svg'
                    },
                    {
                        title: 'boy soccer',
                        value: 7,
                        body: 'img/boy.svg'
                    },
                    {
                        title: 'boy hockey',
                        value: 8,
                        body: 'img/boy.svg'
                    }
                ]
            },
            {
                title: 'girl',
                value: 5,
                bodyStyleList: [
                    {
                        title: 'girl basic',
                        value: 0,
                        body: 'img/girl.svg'
                    },
                    {
                        title: 'girl football',
                        value: 1,
                        body: 'img/girl.svg'
                    },
                    {
                        title: 'girl baseball',
                        value: 2,
                        body: 'img/girl.svg'
                    },
                    {
                        title: 'girl tennis',
                        value: 3,
                        body: 'img/girl.svg'
                    },
                    {
                        title: 'girl swimming',
                        value: 4,
                        body: 'img/girl.svg'
                    },
                    {
                        title: 'girl backetball',
                        value: 5,
                        body: 'img/girl.svg'
                    },
                    {
                        title: 'girl ice skating',
                        value: 6,
                        body: 'img/girl.svg'
                    },
                    {
                        title: 'girl soccer',
                        value: 7,
                        body: 'img/girl.svg'
                    },
                    {
                        title: 'girl hockey',
                        value: 8,
                        body: 'img/girl.svg'
                    }
                ]
            },
            {
                title: 'baby',
                value: 6,
                bodyStyleList: [
                    {
                        title: 'boy basic',
                        value: 0,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy football',
                        value: 1,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy baseball',
                        value: 2,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy tennis',
                        value: 3,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy swimming',
                        value: 4,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy backetball',
                        value: 5,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy ice skating',
                        value: 6,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy soccer',
                        value: 7,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy hockey',
                        value: 8,
                        body: 'img/body.jpg'
                    }
                ]
            },
            {
                title: 'senior male',
                value: 7,
                bodyStyleList: [
                    {
                        title: 'boy basic',
                        value: 0,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy football',
                        value: 1,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy baseball',
                        value: 2,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy tennis',
                        value: 3,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy swimming',
                        value: 4,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy backetball',
                        value: 5,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy ice skating',
                        value: 6,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy soccer',
                        value: 7,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy hockey',
                        value: 8,
                        body: 'img/body.jpg'
                    }
                ]
            },
            {
                title: 'senior female',
                value: 8,
                bodyStyleList: [
                    {
                        title: 'boy basic',
                        value: 0,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy football',
                        value: 1,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy baseball',
                        value: 2,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy tennis',
                        value: 3,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy swimming',
                        value: 4,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy backetball',
                        value: 5,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy ice skating',
                        value: 6,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy soccer',
                        value: 7,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy hockey',
                        value: 8,
                        body: 'img/body.jpg'
                    }
                ]
            },
            {
                title: 'dog',
                value: 9,
                bodyStyleList: [
                    {
                        title: 'dog basic',
                        value: 0,
                        body: 'img/dog.svg'
                    },
                    {
                        title: 'dog football',
                        value: 1,
                        body: 'img/dog.svg'
                    },
                    {
                        title: 'dog baseball',
                        value: 2,
                        body: 'img/dog.svg'
                    },
                    {
                        title: 'dog tennis',
                        value: 3,
                        body: 'img/dog.svg'
                    },
                    {
                        title: 'dog swimming',
                        value: 4,
                        body: 'img/dog.svg'
                    },
                    {
                        title: 'dog backetball',
                        value: 5,
                        body: 'img/dog.svg'
                    },
                    {
                        title: 'dog ice skating',
                        value: 6,
                        body: 'img/dog.svg'
                    },
                    {
                        title: 'dog soccer',
                        value: 7,
                        body: 'img/dog.svg'
                    },
                    {
                        title: 'dog hockey',
                        value: 8,
                        body: 'img/dog.svg'
                    }
                ]
            },
            {
                title: 'cat',
                value: 10,
                bodyStyleList: [
                    {
                        title: 'boy basic',
                        value: 0,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy football',
                        value: 1,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy baseball',
                        value: 2,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy tennis',
                        value: 3,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy swimming',
                        value: 4,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy backetball',
                        value: 5,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy ice skating',
                        value: 6,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy soccer',
                        value: 7,
                        body: 'img/body.jpg'
                    },
                    {
                        title: 'boy hockey',
                        value: 8,
                        body: 'img/body.jpg'
                    }
                ]
            }
        ];

        vm.selectedCategory = vm.categoryList[4];
        vm.selectedBodyStyle = vm.selectedCategory.bodyStyleList[0];
        vm.isPlacedCaricature = false;
        vm.isShowPopup = false;

        vm.getBackgroundStyle = getBackgroundStyle;
        vm.placeCaricature = placeCaricature;
        vm.closePopup = closePopup;
        vm.showPopup = showPopup;

        // this is used later in the resizing and gesture demos
        window.dragMoveListener = dragMoveListener;

        init();
        function init() {
            interact('.resize-drag')
                .draggable({
                    onmove: window.dragMoveListener
                })
                .resizable({
                    preserveAspectRatio: true,
                    edges: { left: true, right: true, bottom: true, top: true }
                })
                .on('resizemove', function (event) {
                    var target = event.target,
                        x = (parseFloat(target.getAttribute('data-x')) || 0),
                        y = (parseFloat(target.getAttribute('data-y')) || 0);

                    // update the element's style
                    target.style.width  = event.rect.width + 'px';
                    target.style.height = event.rect.height + 'px';

                    // translate when resizing from top or left edges
                    x += event.deltaRect.left;
                    y += event.deltaRect.top;

                    target.style.webkitTransform = target.style.transform =
                        'translate(' + x + 'px,' + y + 'px)';

                    target.setAttribute('data-x', x);
                    target.setAttribute('data-y', y);
                    // target.textContent = Math.round(event.rect.width) + '×' + Math.round(event.rect.height);
                    target.innerHTML = "<i class='resize-drag__corner resize-drag__corner--top-left'></i>" +
                        "<i class='resize-drag__corner resize-drag__corner--top-right'></i>" +
                        "<i class='resize-drag__corner resize-drag__corner--bottom-left'></i>" +
                        "<i class='resize-drag__corner resize-drag__corner--bottom-right'></i>";
                });
        }

        function dragMoveListener (event) {
            var target = event.target,
                // keep the dragged position in the data-x/data-y attributes
                x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

            // translate the element
            target.style.webkitTransform =
                target.style.transform =
                    'translate(' + x + 'px, ' + y + 'px)';

            // update the posiion attributes
            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
        }

        function getBackgroundStyle () {
            return {
                'background-image': 'url(' + vm.filterFace + ')'
            }
        }

        function placeCaricature() {
            vm.isPlacedCaricature = !vm.isPlacedCaricature;
        }

        function closePopup(answer) {
            vm.isShowPopup = false;
        }

        function showPopup() {
            vm.isShowPopup = true;
        }

        $scope.$watch("vm.selectedCategory ", function handleFooChange( newValue ) {
            vm.selectedCategory = newValue;
            vm.selectedBodyStyle = vm.selectedCategory.bodyStyleList[0];
            }
        );
    }

    angular
        .module('myApp')
        .component('homeStepTwoComponent', HomeStepTwoComponent)
        .controller('HomeStepTwoCtrl', HomeStepTwoCtrl);
})();