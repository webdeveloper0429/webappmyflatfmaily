(function () {
    'use strict';

    var FaceFilterComponent = {
        bindings: {
            originalFace: "=",
            filterFace: '=',
            onPreviousStep: '&',
            onNextStep: '&'
        },
        templateUrl: 'views/home/faceFilter.template.html',
        controller: 'FaceFilterCtrl',
        controllerAs: 'vm'
    };

    FaceFilterCtrl.$inject = ['$timeout'];

    function FaceFilterCtrl($timeout) {
        var vm = this;

        var canvas,copy,filter;
        vm.isPortraitFace = false;

        vm.goNext = goNext;


        function init() {
            canvas = document.getElementById("canvas-filter");
            copy = document.createElement("canvas");
            filter = new tsunami.filters.ColorHalftoneFilter();

            var image = new Image();
            image.onload = function () {
                // var size = tsunami.geom.Ratio.scaleToFill({width:image.width, height:image.height}, {width:canvas.width, height:canvas.height});
                // var size2 = tsunami.geom.Ratio.scaleToFit({width:canvas.width, height:canvas.height}, {width:image.width, height:image.height});

                var context = copy.getContext("2d");
                context.canvas.width = 150;
                context.canvas.height = 175;
                context.fillStyle = "#FFFFFF";
                context.fillRect(0, 0, context.canvas.width, context.canvas.height);
                // context.drawImage(image, (image.width - size2.width) / 2, (image.height - size2.height) / 2, size2.width, size2.height, 0, 0, canvas.width, canvas.height);
                // context.drawImage(image, 0, 0, image.width, image.height);


                var rotX = context.canvas.width / image.width;
                var rotY = context.canvas.height / image.height;
                var ratio = rotX;

                if (image.width > image.height) {
                    ratio = rotX;
                    vm.isPortraitFace = false;
                } else {
                    ratio = rotY;
                    vm.isPortraitFace = true;
                }

                context.drawImage(image, 0,0, image.width * ratio,
                    image.height * ratio);

                context.image = image;

                applyFilter();
            };

            image.src = vm.originalFace;
        }
        init();

        function applyFilter() {
            var context = canvas.getContext("2d");
            context.drawImage(copy, 0, 0, canvas.width, canvas.height);
            setTimeout(waitForRedraw.bind(this), 100);
        }

        function waitForRedraw() {
            // var context = canvas.getContext("2d");
            // filter.pixelsPerPoint = eval(2); // pixel per point
            // filter.applyFilter(context);
            //
            // var imgd = context.getImageData(0, 0, canvas.width, canvas.height);
            // var pix = imgd.data;
            // for (var i = 0, n = pix.length; i < n; i += 4) {
            //     var grayscale = pix[i ] * .3 + pix[i+1] * .59 + pix[i+2] * .11;
            //     pix[i ] = grayscale; // red
            //     pix[i+1] = grayscale; // green
            //     pix[i+2] = grayscale; // blue
            // }
            // context.putImageData(imgd, 0, 0);



            var finishCanvas = document.createElement("canvas");
            finishCanvas.width = canvas.width;
            finishCanvas.height = canvas.height;

            var finishCtx = finishCanvas.getContext("2d");
            finishCtx.fillStyle = "#ffffff";
            finishCtx.fillRect(0, 0, canvas.width, canvas.height);

            var context = canvas.getContext("2d");
            filter.pixelsPerPoint = eval(2);
            filter.applyFilter(context);

            var imgd = context.getImageData(0, 0, canvas.width, canvas.height);
            var finishImgd = finishCtx.getImageData(0, 0, finishCanvas.width, finishCanvas.height);

            var pix = imgd.data;


            var emptyPixelCount = 0;
            var isMakeTranparent = true;
            for (var i = 0, n = pix.length; i < n; i += 4) {
                var grayscale = pix[i] * .3 + pix[i+1] * .59 + pix[i+2] * .11;

                if (pix[i] == 255 && pix[i+1] == 255 && pix[i+2] == 255) { // if white

                    if (isMakeTranparent) {
                        finishImgd.data[i+3] = 0; //make transparent
                    } else {
                        emptyPixelCount++;
                        if (emptyPixelCount > 40) {
                            isMakeTranparent = true;
                            i-= emptyPixelCount * 4;
                            emptyPixelCount = 0;
                        }
                    }

                } else {
                    pix[i] = grayscale; // red
                    finishImgd.data[i] = grayscale;
                    pix[i+1] = grayscale; // green
                    finishImgd.data[i+1] = grayscale;
                    pix[i+2] = grayscale; // blue
                    finishImgd.data[i+2] = grayscale;

                    isMakeTranparent = false;
                    emptyPixelCount = 0;
                }
            }

            context.putImageData(imgd, 0, 0);
            finishCtx.putImageData(finishImgd, 0, 0);

            vm.filterFace = finishCanvas.toDataURL("image/png", undefined);
            $timeout(function () {
                vm.filterFace = finishCanvas.toDataURL("image/png", undefined);
            }, 10);



        }

        function goNext() {
            $timeout(function () {
                vm.onNextStep();
            }, 10);
        }

    }

    angular
        .module('myApp')
        .component('faceFilterComponent', FaceFilterComponent)
        .controller('FaceFilterCtrl', FaceFilterCtrl);
})();