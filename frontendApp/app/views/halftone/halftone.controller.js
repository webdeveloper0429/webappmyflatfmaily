(function () {
    "use strict";

    angular
        .module("myApp")
        .controller("HalftoneCtrl", HalftoneCtrl);

    HalftoneCtrl.$inject = ['$scope', '$timeout'];

    function HalftoneCtrl($scope, $timeout) {

        var vm = this;

        var container,canvas,copy,filter,image;

        var range = {
            value: 4
        };
        vm.imageFilter = null;



        function init() {
            container = document.getElementById("container");
            canvas = document.getElementById("canvas");
            copy = document.createElement("canvas");
            filter = new tsunami.filters.ColorHalftoneFilter();

            window.onresize = windowResize.bind(this);
            windowResize(null);
        }
        init();



        // start Picture Preview
        $scope.imageUpload = function (event) {
            var file = event.target.files[0];

            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(file);
        }

        function imageIsLoaded (e) {
            $scope.$apply(function () {
                image = new Image();
                image.onload = function () {
                    var size = tsunami.geom.Ratio.scaleToFill({width:image.width, height:image.height}, {width:canvas.width, height:canvas.height});
                    var size2 = tsunami.geom.Ratio.scaleToFit({width:canvas.width, height:canvas.height}, {width:image.width, height:image.height});

                    var context = copy.getContext("2d");
                    context.drawImage(image, (image.width - size2.width) / 2, (image.height - size2.height) / 2, size2.width, size2.height, 0, 0, canvas.width, canvas.height);

                    applyFilter();
                };


                image.src = event.target.result;
                // image.src = bla;
            });
        }



        function windowResize(event) {
            var stageWidth = Math.min(400, window.innerWidth);
            var stageHeight = 500;

            container.style.width = stageWidth + "px";
            container.style.height = stageHeight + "px";

            canvas.width = stageWidth -  20;
            canvas.height = stageHeight -  20;

            var ctx = canvas.getContext("2d");
            ctx.fillStyle = "#ffffff";
            ctx.fillRect(0, 0, canvas.width, canvas.height);

            copy.width = canvas.width;
            copy.height = canvas.height;
        }

        function applyFilter() {
            var context = canvas.getContext("2d");
            context.drawImage(copy, 0, 0, canvas.width, canvas.height);
            setTimeout(waitForRedraw.bind(this), 100);
        }

        function waitForRedraw() {
            var finishCanvas = document.createElement("canvas");
            finishCanvas.width = canvas.width;
            finishCanvas.height = canvas.height;

            var finishCtx = finishCanvas.getContext("2d");
            finishCtx.fillStyle = "#ffffff";
            finishCtx.fillRect(0, 0, canvas.width, canvas.height);

            var context = canvas.getContext("2d");
            filter.pixelsPerPoint = eval(range.value);
            filter.applyFilter(context);

            var imgd = context.getImageData(0, 0, canvas.width, canvas.height);
            var finishImgd = finishCtx.getImageData(0, 0, finishCanvas.width, finishCanvas.height);

            var pix = imgd.data;

            var emptyPixelCount = 0;
            var isMakeTranparent = true;
            for (var i = 0, n = pix.length; i < n; i += 4) {
                var grayscale = pix[i] * .3 + pix[i+1] * .59 + pix[i+2] * .11;

                if (pix[i] == 255 && pix[i+1] == 255 && pix[i+2] == 255) { // if white

                    if (isMakeTranparent) {
                        finishImgd.data[i+3] = 0; //make transparent
                    } else {
                        emptyPixelCount++;
                        if (emptyPixelCount > 10) {
                            isMakeTranparent = true;
                            i-= emptyPixelCount * 4;
                            emptyPixelCount = 0;
                        }
                    }

                } else {
                    pix[i] = grayscale; // red
                    finishImgd.data[i] = grayscale;
                    pix[i+1] = grayscale; // green
                    finishImgd.data[i+1] = grayscale;
                    pix[i+2] = grayscale; // blue
                    finishImgd.data[i+2] = grayscale;

                    isMakeTranparent = false;
                    emptyPixelCount = 0;
                }
            }

            context.putImageData(imgd, 0, 0);
            finishCtx.putImageData(finishImgd, 0, 0);

            vm.imageFilter = finishCanvas.toDataURL("image/png", undefined);
            $timeout(function () {
                vm.imageFilter = finishCanvas.toDataURL("image/png", undefined);
            }, 10);
        }
    }
})();